package com.halas.second_task;

public class Container {
    private static final int MAX_SIZE = 10;

    private static int size;

    private String[] array ;
    private int index;

    public Container(){
        array = new String[MAX_SIZE];
        index=0;
        size=MAX_SIZE;
    }
    public Container(int size){
        Container.size=size*2;
        array = new String[Container.size];
        index=0;
    }

    private boolean isCorrect(){
        return index<size;
    }

    private void reInit(){
        String[] clone;
        clone = array.clone();

        size *= 2;
        array = new String[size];
        for(int i=0;i<clone.length;i++){
            array[i]=clone[i];
        }
    }

    public void add(String obj){
        if(isCorrect()){
            array[index] = obj;
            index++;
        }
        else{
            reInit();
            array[index] = obj;
            index++;
        }
    }
    public void setIndexToStart(){
        index=0;
    }
    public String getElement(){
        return array[index];
    }

    public void showAll(){
        for(int i=0;i<size;i++){
            if(array[i]!=null){
                System.out.println("value["+i+"]: "+array[i]);
            }
            else{
                break;
            }
        }
    }
}
