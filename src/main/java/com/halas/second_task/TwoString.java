package com.halas.second_task;

public class TwoString {
    private String country;
    private String capital;
    public TwoString(){

    }

    public TwoString(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public void setAll(TwoString o1){
        this.country = o1.country;
        this.capital = o1.capital;
    }

    @Override
    public String toString() {
        return "Country: "
                + country
                + ", Capital: "
                + capital;
    }
}
