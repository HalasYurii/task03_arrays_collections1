package com.halas.second_task.MyComparators;

import com.halas.second_task.TwoString;

import java.util.Comparator;

public class CapitalComparator<T> implements Comparator<TwoString> {

    @Override
    public int compare(TwoString o1, TwoString o2) {
        return o1.getCapital().compareTo(o2.getCapital());
    }
}
