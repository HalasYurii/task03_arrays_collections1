package com.halas.second_task;

import com.halas.second_task.MyComparators.CapitalComparator;
import com.halas.second_task.MyComparators.CountryComparator;

import java.util.*;

public class Run_second {
    public static TwoString getRandomCountry() {

        Random rnd = new Random();
        final int max = 10;
        int choose = rnd.nextInt(max);//від 0 до 9 [0;9]

        TwoString element = new TwoString();
        switch (choose) {
            case 0:
                element.setCountry("Afghanistan");
                element.setCapital("Kabul");
                break;
            case 1:
                element.setCountry("Albania");
                element.setCapital("Tirana");
                break;
            case 2:
                element.setCountry("Canada");
                element.setCapital("Ottawa");
                break;
            case 3:
                element.setCountry("China");
                element.setCapital("Beijing");
                break;
            case 4:
                element.setCountry("Venezuela");
                element.setCapital("Caracas");
                break;
            case 5:
                element.setCountry("Egypt");
                element.setCapital("Cairo");
                break;
            case 6:
                element.setCountry("Mexico");
                element.setCapital("Mexico City");
                break;
            case 7:
                element.setCountry("Peru");
                element.setCapital("Lima");
                break;
            case 8:
                element.setCountry("Switzerland");
                element.setCapital("Bern");
                break;
            case 9:
                element.setCountry("Zimbabwe");
                element.setCapital("Harare");
                break;
             default:
                 element.setCountry("null");
                 element.setCapital("null");
                 break;
        }
        return element;
    }
    public static void taskSecondStart(){
        Container row = new Container();

        row.add("Yura");
        row.add("2");
        row.add("3");
        row.add("4");
        row.add("5");
        row.add("6");
        row.add("7");
        row.add("8");
        row.add("9");
        row.add("10");
        row.add("11");
        row.add("12");
        row.add("13");
        row.add("144");
        row.add("155");
        row.showAll();


        final int size = 5;
        TwoString []file = new TwoString[size];

        System.out.println("Before sorting:");
        for(int i=0;i<size;i++){
            file[i] = new TwoString();
            file[i].setAll(getRandomCountry());
            System.out.println(file[i]);
        }

        Arrays.sort(file,new CountryComparator());

        System.out.println("\n\nAfter sorting country:");
        for (int i=0;i<size;i++){
            System.out.println(file[i]);
        }


        Arrays.sort(file,new CapitalComparator());

        System.out.println("\n\nAfter sorting capital:");
        for (int i=0;i<size;i++){
            System.out.println(file[i]);
        }

        String key = file[2].getCapital();
        int a = Arrays.binarySearch(file,new TwoString("Peru","Lima"),new CapitalComparator());
        System.out.println("Index with key \""+key+"\": "+a);

        System.out.println("\n\nDeque:");
        Deque<String> dec= new ArrayDeque<String>();
        dec.add("first");
        dec.addFirst("second");
        dec.add("third");

        for(Iterator itr = dec.iterator(); itr.hasNext();)  {
            System.out.println(itr.next());
        }
    }


}
