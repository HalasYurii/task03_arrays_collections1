package com.halas.Game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Player {
    private String name;
    private int damage;
    private List<Artifact> art;
    private boolean isWon = false;


    public Player() {
        art = new ArrayList<Artifact>();
    }

    public Player(String name) {
        this.name = name;
        art = new ArrayList<Artifact>();
    }

    public boolean isWon() {
        return isWon;
    }

    public void setWon(boolean won) {
        isWon = won;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void learnArtifacts() {
        int artDmg = 0;
        for (Artifact e : art) {
            if (!e.isLearnt()) {
                artDmg += e.getBonus();
                e.setLearnt(true);
            }
        }
        damage += artDmg;
    }

    public void showAboutArtifacts() {
        System.out.println("Your artifacts: ");
        for (Artifact e : art) {
            System.out.println(e);
        }
    }

    public void setAtrifact(Artifact arty) {
        this.art.add(arty);
    }

    protected void setDamage(int dmg) {
        this.damage = dmg;
    }

    public static int getRandomInt(int from, int to) {
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    public String getName() {
        return name;
    }

    public int getDamage() {
        return damage;
    }

    @Override
    public String toString() {
        return "{" +
                "name: '" + name + '\'' +
                ", damage: " + damage +
                ", artifacts: " + art +
                '}';
    }
}
