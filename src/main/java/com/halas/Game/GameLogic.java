package com.halas.Game;

import com.halas.Functional;
import com.halas.Game.Players.Hero;
import com.halas.Game.Players.Monster;

import java.util.*;

import static com.halas.Game.Player.getRandomInt;

public class GameLogic {
    private static final int COUNT_OF_DOORS = 10;
    private static final int MIN_MONSTERS = 0;
    private static final int MAX_MONSTERS = COUNT_OF_DOORS;
    private static Scanner scanner = new Scanner(System.in);

    private int countOfClearRoom;
    private boolean heroDead;
    private List doors;
    private Player hero;
    private String choose;

    private int countMonsters;
    private int countAtrifacts;


    //menu
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;

    public GameLogic() {

        if (COUNT_OF_DOORS > 100) {
            System.out.println("Bad program!");
            System.exit(0);
        }

        init();
        startWish();
        addMonstersAndArt();
        initializeMenu();
    }


    private void initializeMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("", "\n\nChoose door, from \"1\", to \"" + COUNT_OF_DOORS + "\"");
        menu.put("101", " 101 - Get info about all doors");
        menu.put("102", " 102 - Get easy way to win");
        menu.put("103", " 103 - Get information where i can lose");
        menu.put("104", " 104 - Get information about your hero");
        menu.put("105", " 105 - Exit");

        methodsMenu.put("101", this::help);
        methodsMenu.put("102", this::easyWin);
        methodsMenu.put("103", this::easyLose);
        methodsMenu.put("104", this::printInfoHero);
        //methodsMenu.put("There must be value from 1 to %count_of_doors%",this::isEnter);
    }

    private void showMenu() {
        for (String value : menu.values()) {
            System.out.println(value);
        }
    }

    private void addMonstersAndArt() {

        //1 - ставити це значення
        //0 - не ставити це значення
        //вирішує рандом
        int setOrNot;
        boolean isFine;
        for (int i = 0, sizeM = 0, sizeA = 0; i < COUNT_OF_DOORS; i++) {
            setOrNot = getRandomInt(0, 1);
            isFine = false;
            if ((setOrNot == 1) && (sizeM < countMonsters)) {
                doors.add(new Monster());
                sizeM++;
                isFine = true;
            } else if (sizeA < countAtrifacts) {
                doors.add(new Artifact());
                sizeA++;
                isFine = true;
            }
            //якщо не встановило щось у двері то
            // повертаємся до них
            if (!isFine) {
                i--;
            }
        }

    }

    private void help() {
        int index = 1;
        for (Object e : doors) {
            System.out.println("\n\nAfter door: " + index);
            if (e instanceof Artifact) {
                System.out.println("Art:  " + e);
            } else if (e instanceof Monster) {
                System.out.println("Monster:  " + e);
            } else {
                System.out.println("Unknown");
            }
            index++;
            System.out.println();
        }
    }

    private int getAllArtDmg() {
        int dmg = 0;
        for (Object e : doors) {
            if (e instanceof Artifact) {
                dmg += ((Artifact) e).getBonus();
            }
        }
        return dmg;
    }


    private void setWayWin(int heroDmg, StringBuilder output) {
        int index = 1;
        for (Object e : doors) {
            if (e instanceof Monster) {
                if ((heroDmg > ((Monster) e).getDamage() && (!((Monster) e).isDead()))) {
                    if (((Monster) e).isCorrect()) {
                        output.append("->choose: \"");
                        output.append(index);
                        output.append("\"");
                        ((Monster) e).setDead(true);
                        setWayWin(heroDmg, output);
                    }
                }
            }
            index++;
        }
    }

    private void easyWin() {
        StringBuilder output = new StringBuilder();

        int index = 1;
        for (Object e : doors) {
            if (e instanceof Artifact) {
                if (((Artifact) e).isCorrect()) {
                    output.append("->choose: \"");
                    output.append(index);
                    output.append("\"  ");
                }
            }
            index++;
        }
        int heroDmg = hero.getDamage() + getAllArtDmg();
        setWayWin(heroDmg, output);

        for (Object monster : doors) {
            if (monster instanceof Monster) {
                ((Monster) monster).setDead(false);
            }
        }
        System.out.println(output);
    }

    private void setWayLose(int heroDmg, StringBuilder output) {
        int index = 1;
        for (Object e : doors) {
            if (e instanceof Monster) {
                if ((heroDmg < ((Monster) e).getDamage() && (!((Monster) e).isDead()))) {
                    if (((Monster) e).isCorrect()) {
                        output.append("->choose: \"");
                        output.append(index);
                        output.append("\"");
                        ((Monster) e).setDead(true);
                        setWayLose(heroDmg, output);
                    }
                }
            }
            index++;
        }
        if (output.toString().length() <= 0) {
            output.append("\nYou so strong, and can easily win this game!\n");
        }
    }

    private void easyLose() {
        StringBuilder output = new StringBuilder();
        int heroDmg = hero.getDamage();
        setWayLose(heroDmg, output);

        for (Object monster : doors) {
            if (monster instanceof Monster) {
                ((Monster) monster).setDead(false);
            }
        }
        System.out.println(output);
    }

    private void startWish() {
        System.out.println("***Game started***");
        System.out.println("The count of artifacts: " + countAtrifacts);
        System.out.println("The count of monsters: " + countMonsters);
        System.out.println("Good luck, have fun!\n");
    }


    private void fight(Object e) {
        System.out.println("You open the door and..");
        System.out.print("There is ");
        if (e instanceof Monster) {
            if (!((Monster) e).isCorrect()) {
                System.out.println("nothing");
            } else {
                System.out.print("monster: ");
                System.out.println(e);
                if (((Monster) e).getDamage() <= hero.getDamage()) {
                    System.out.println("You clear this room!");
                    ((Monster) e).clear();
                    countOfClearRoom++;
                } else {
                    System.out.println("***GAME OVER***\nYou lost!\n");
                    heroDead = true;
                }
            }
        } else if (e instanceof Artifact) {
            if (!((Artifact) e).isCorrect()) {
                System.out.println("nothing");
            } else {
                System.out.print("artifact: ");
                System.out.println("You got stronger by " + ((Artifact) e).getBonus() + " points!");

                Artifact copy = new Artifact((Artifact) e);
                hero.setAtrifact(copy);
                hero.learnArtifacts();

                System.out.println("Now your damage is: " + hero.getDamage());
                ((Artifact) e).clear();
                countOfClearRoom++;
            }
        }
        if (countOfClearRoom == COUNT_OF_DOORS) {
            hero.setWon(true);
        }
    }


    private boolean isEnter() {
        Object room;

        for (int i = 1; i <= COUNT_OF_DOORS; i++) {
            if (Integer.toString(i).equals(choose)) {
                room = doors.get(i - 1);
                fight(room);
                return true;
            }

        }
        return false;
    }

    private void init() {
        countOfClearRoom = 0;
        heroDead = false;
        doors = new ArrayList<>();
        hero = new Hero();
        countMonsters = getRandomInt(MIN_MONSTERS, MAX_MONSTERS);
        countAtrifacts = COUNT_OF_DOORS - countMonsters;
    }

    private boolean isGameOver() {
        if (heroDead) {
            return true;
        } else {
            if (hero.isWon()) {
                System.out.println("**YOU WON THIS GAME**\nGood job!");
                return true;
            }
        }
        return false;

    }

    private void printInfoHero() {
        System.out.println("Your hero: " + hero);
    }

    public static void taskGame() {
        GameLogic game = new GameLogic();
        do {
            game.showMenu();
            System.out.println("Please, select menu point.");
            game.choose = scanner.nextLine();
            try {
                game.methodsMenu.get(game.choose).start();
            } catch (Exception e) {
                if (game.isEnter()) {
                    if (game.isGameOver()) {
                        break;
                    }
                } else {
                    System.out.println("Error, please try again.\n");
                }
            }
        } while (!game.choose.equals("105"));
    }


}
