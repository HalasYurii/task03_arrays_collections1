package com.halas.Game;

import static com.halas.Game.Player.getRandomInt;

public class Artifact {
    private String name;
    private int bonus;
    private boolean isLearnt;

    private static final int min = 10;
    private static final int max = 80;


    public Artifact() {
        bonus = getRandomInt(min, max);
        chooseName();
        isLearnt=false;
    }
    public Artifact(Artifact art){
        this.name=art.name;
        this.bonus=art.bonus;
        isLearnt=false;
    }

    public String getName() {
        return name;
    }

    private void chooseName(){
        if(bonus<20){
            this.name = "Phase boots";
        }else if(bonus<40){
            this.name = "Maelstrom";
        } else if(bonus<60){
            this.name = "Mjolnir";
        } else {
            this.name = "Divine Rapier";
        }

    }
    public boolean isLearnt() {
        return isLearnt;
    }

    public void setLearnt(boolean learnt) {
        isLearnt = learnt;
    }

    public int getBonus() {
        return bonus;
    }

    public boolean isCorrect(){
        return (!name.equals("")) || (bonus != 0);
    }
    public void clear(){
        name="";
        bonus=0;

    }

    @Override
    public String toString() {
        return "Name: \"" + name
                + "\", bonus: +" + bonus;
    }
}
