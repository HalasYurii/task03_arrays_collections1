package com.halas.Game.Players;

import com.halas.Game.Player;

public class Hero extends Player {
    private static int DMG = 25;

    public Hero(){
        super();
        setName("John");
        setDamage(DMG);
    }

}
