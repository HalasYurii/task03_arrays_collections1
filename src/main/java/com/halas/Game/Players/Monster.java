package com.halas.Game.Players;

import com.halas.Game.Player;

public class Monster extends Player {
    private static final int min = 5;
    private static final int max = 100;
    private boolean isDead = false;


    public Monster() {
        super();
        setDamage(getRandomInt(min, max));
        chooseName();
    }

    private void chooseName() {
        if (getDamage() < 15) {
            setName("Goblin");
        } else if (getDamage() < 35) {
            setName("Satyr");
        } else if (getDamage() < 65) {
            setName("Dragon");
        } else if (getDamage() < 85) {
            setName("Ember Spirit");
        } else {
            setName("Pudge");
        }

    }

    public void clear() {
        setName("");
        setDamage(0);
    }

    public void setDead(boolean flag) {
        isDead = flag;
    }

    public boolean isDead() {
        return isDead;
    }

    public boolean isCorrect() {
        return (!getName().equals("")) || (getDamage() != 0);
    }

    @Override
    public String toString() {
        return "Name: \"" + getName()
                + "\", damage: " + getDamage();
    }
}
