package com.halas.first_task;

public class MyPriorityQueue<T> {

    private T value;
    private int priority;

    private static int size=0;
    private static MyPriorityQueue begin;
    private static MyPriorityQueue tmp;
    private MyPriorityQueue next;


    public MyPriorityQueue() {
        tmp = begin;
    }

    private void addBegin(Object e, int priority) {
        MyPriorityQueue t = new MyPriorityQueue();
        t.value = e;
        t.priority = priority;
        t.next = begin;
        begin = t;
    }

    public void add(Object e, int priority) {
        if (begin == null) {
            begin = new MyPriorityQueue();
            begin.value = e;
            begin.priority = priority;
            begin.next = null;
        } else if (begin.priority > priority) {
            addBegin(e, priority);
        } else {
            MyPriorityQueue tmp = begin;
            while ((tmp.next != null) && (tmp.priority < priority)) {
                tmp = tmp.next;
            }
            tmp.next = new MyPriorityQueue();
            tmp = tmp.next;
            tmp.value = e;
            tmp.priority = priority;
            tmp.next = null;
        }
        size++;
    }

    public int getSize() {
        return size;
    }

    public Object getValue() {
        Object c=tmp.value;
        if(canPop()){
            tmp = tmp.next;
        }

        return c;
    }

    public boolean canPop() {
        return tmp != null;

    }

    public void setPointerToStart() {
        tmp = begin;
    }

    public void show() {
        MyPriorityQueue tmp = begin;
        while (tmp != null) {
            System.out.println("value: " + tmp.value);
            tmp = tmp.next;
        }
    }

}
