package com.halas.first_task;

public class Run_first {
    public static void taskFirstStart(){
        //виводить від найменшого пріоритету

        MyPriorityQueue<String> list = new MyPriorityQueue<String>();

        list.add("first",0);
        list.add("second",-1);
        list.add("third",2);

        int size=list.getSize();
        for(int i=0;i<size;i++){
            if(list.canPop()){
                System.out.print("value: ");
                System.out.println(list.getValue());
            }
        }
    }
}
