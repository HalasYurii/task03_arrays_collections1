package com.halas.third_task;


import java.util.Random;

public class Run_third {

    public static void fillRand(int[] mas) {
        Random rnd = new Random();
        final int min = 1;
        final int max = 10;
        for (int i = 0; i < mas.length; i++) {
            mas[i] = min + rnd.nextInt(max);
        }
    }

    public static void showMass(int[] mas) {
        System.out.print("Massive: [");
        for (int i = 0; i < mas.length; i++) {
            if (i == mas.length - 1) {
                System.out.print(mas[i]);
            } else {
                System.out.print(mas[i] + ", ");
            }
        }
        System.out.println("];");
    }


    private static boolean isUniq(int[] mas, int index) {
        boolean notUniq = false;
        for (int j = index + 1; j < mas.length; j++) {
            if (mas[index] == mas[j]) {
                notUniq = true;
                break;
            }
        }
        return !notUniq;
    }

    private static boolean isAtMassiveNotUniq(int value, int[] second, int secondStart) {
        boolean flag = false;
        for (int j = secondStart; j < second.length; j++) {
            if (value == second[j]) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    private static boolean isAtMassive(int[] mas, int value) {
        for (int i = 0; i < mas.length; i++) {
            if (!isAtMassiveNotUniq(mas[i], mas, i + 1)) {
                if (mas[i] == value) {
                    return true;
                }
            }
        }
        return false;
    }


    public static int[] areInBoth(int[] first, int[] second) {
        int size = 0;
        for (int i = 0; i < first.length; i++) {
            if (!isAtMassiveNotUniq(first[i], first, i + 1)) {
                if (isAtMassive(second, first[i])) {
                    size++;
                }
            }
        }
        int[] result = new int[size];

        for (int i = 0, k = 0; i < first.length; i++) {
            if (!isAtMassiveNotUniq(first[i], first, i + 1)) {
                if (isAtMassive(second, first[i])) {
                    result[k] = first[i];
                    k++;
                }
            }
        }

        return result;
    }

    public static int[] areOnlyInFirst(int[] first, int[] second) {
        int[] result;
        int sizeUniq = 0;
        int sizeRes = 0;
        for (int i = 0; i < first.length; i++) {
            if (isUniq(first, i)) {
                sizeUniq++;
            }
        }
        int[] masUniqFirst = new int[sizeUniq];
        for (int i = 0, k = 0; i < first.length; i++) {
            if (isUniq(first, i)) {
                masUniqFirst[k] = first[i];
                k++;
            }
        }

        for (int i = 0; i < sizeUniq; i++) {
            if (!isAtMassiveNotUniq(masUniqFirst[i], second, 0)) {
                sizeRes++;
            }
        }

        result = new int[sizeRes];
        for (int i = 0, k = 0; i < sizeUniq; i++) {
            if (!isAtMassiveNotUniq(masUniqFirst[i], second, 0)) {
                result[k] = masUniqFirst[i];
                k++;
            }
        }
        return result;
    }

    private static int getIndex(int[] mas, int element) {
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] == element) {
                return i;
            }
        }
        return -1;
    }

    private static int[] deleteElementByIndex(int[] mas, int index) {
        int[] result = new int[mas.length - 1];
        for (int i = index; i < mas.length - 1; i++) {
            mas[i] = mas[i + 1];
        }
        if (result.length >= 0) System.arraycopy(mas, 0, result, 0, result.length);
        return result;
    }

    private static int[] deleteElementByValue(int[] mas, int element) {
        if (isAtMassiveNotUniq(element, mas, 0)) {
            int[] result = new int[mas.length - 1];
            for (int i = getIndex(mas, element); i < mas.length - 1; i++) {
                mas[i] = mas[i + 1];
            }
            if (result.length >= 0) System.arraycopy(mas, 0, result, 0, result.length);
            return result;
        } else {
            System.out.println("Can't find this element for deleting!\n");
        }
        return mas;
    }

    public static int[] deleteMoreThenTwo(int[] mas) {
        final int maxPovt = 2;
        int count;

        for (int i = 0; i < mas.length; i++) {
            count = 1;
            for (int j = i + 1; j < mas.length; j++) {
                if (mas[i] == mas[j]) {
                    count++;
                    if (count == maxPovt) {
                        int delete = mas[i];
                        while (isAtMassiveNotUniq(delete, mas, 0)) {
                            mas = deleteElementByValue(mas, delete);
                        }
                        i = 0;
                    }
                }
            }
        }
        return mas;
    }

    public static int[] deleteRepeatInRow(int[] mas) {
        int j;

        for (int i = 0; i < mas.length-1; i++) {
            j = i + 1;
            while ((j < mas.length) && (mas[i] == mas[j])) {
                mas = deleteElementByIndex(mas, j);
            }
        }
        return mas;
    }

    //FUNCTION START
    public static void taskThirdStart() {
        final int sizeF = 10;
        final int sizeS = 12;
        int[] arrayFirst = new int[sizeF];
        int[] arraySecond = new int[sizeS];
        int[] result;

        fillRand(arrayFirst);
        fillRand(arraySecond);

        System.out.print("First:  ");
        showMass(arrayFirst);

        System.out.print("Second:  ");
        showMass(arraySecond);


        System.out.print("Are in Both:  ");
        result = areInBoth(arrayFirst, arraySecond);
        showMass(result);


        System.out.print("Are only in First:  ");
        result = areOnlyInFirst(arrayFirst, arraySecond);
        showMass(result);


        System.out.print("First delete more then 2:  ");
        result = deleteMoreThenTwo(arrayFirst);
        showMass(result);


        System.out.print("Second after delete repeatInRow:  ");
        result = deleteRepeatInRow(arraySecond);
        showMass(result);
    }
}
