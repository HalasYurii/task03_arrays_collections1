import com.halas.Functional;
import com.halas.Game.GameLogic;
import com.halas.first_task.Run_first;
import com.halas.second_task.Run_second;
import com.halas.third_task.Run_third;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;


public class Main {
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;

    private static final Scanner scanner = new Scanner(System.in);

    public Main() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - First task(Deque)");
        menu.put("2", " 2 - Second task(Arrays)");
        menu.put("3", " 3 - Third task(Massive)");
        menu.put("4", " 4 - Game");
        menu.put("5", " 5 - Exit");


        methodsMenu.put("1", Run_first::taskFirstStart);
        methodsMenu.put("2", Run_second::taskSecondStart);
        methodsMenu.put("3", Run_third::taskThirdStart);
        methodsMenu.put("4", GameLogic::taskGame);
    }

    private void showMenu() {
        for (String value : menu.values()) {
            System.out.println(value);
        }
    }

    public static void main(String[] args) {

        Main main = new Main();
        String keyMenu;

        do {
            main.showMenu();
            keyMenu = scanner.nextLine();
            System.out.println("Please, select menu point.");
            try {
                main.methodsMenu.get(keyMenu).start();
            } catch (Exception e) {
                System.out.println("Error, please try again!");
            }
        } while (!keyMenu.equals("5"));
    }
}
